function Controller() {
  
  var myCard = new Card();

  this.get_card = function(){
    return myCard;
  }  
	/* MODEL FOR DEMO */

    myCard.setValue("education_model", [ [{"institute" : "DIIT", 
                                     "diplom" : "bachelour",
                                     "year" : "2012",
                                     "special" : "Information security",
                                     "qualify" : "master",
                                     "education_form" : "day"}
                                    ], 
                                    [ {"institute_post" : "DIIT",
                                       "year_post" : "2012",
                                       "degree" : "specialist"},
                                       {"institute_post" : "DNU",
                                       "year_post" : "2011",
                                       "degree" : "magister"}
                                    ] 
                                  ]); 
    myCard.setValue("family_model", [ {"family_degree" : "sister",
                                 "PIB" : "Kravchenko Olena Olegovna",
                                 "year_birthday" : "2000"},
                                 {"family_degree" : "father",
                                 "PIB" : "Vasily Vasilievich",
                                 "year_birthday" : "1968"},
                                 {"family_degree" : "mother",
                                 "PIB" : "Iuliia Vladislavovna",
                                 "year_birthday" : "1968"} ]);

    myCard.setValue("last_name", "Kravchenko", "dont_validate");
    myCard.setValue("first_name", "Evgeny", "dont_validate");
    myCard.setValue("patronymic", "Vasilievich", "dont_validate");
    myCard.setValue("citizen", "ukraine", "dont_validate");
    myCard.setValue("pasport_series", "AN", "dont_validate");
    myCard.setValue("pasport_number", "594343", "dont_validate");
    myCard.setValue("education", "baze high", "dont_validate");
    myCard.setValue("day_birth", "30","dont_validate");
    myCard.setValue("month_birth", "01","dont_validate");
    myCard.setValue("year_birth", "1991","dont_validate");
    myCard.setValue("sex", "female","dont_validate");
    myCard.setValue("army", "true","dont_validate");
    myCard.setValue("partmen", "Dnepropetrovskaya obl, Dnepr city, st. \
    Lazaryana,2, hostel 3, room 18.","dont_validate");
    myCard.setValue("pasport_partmen", "Dnepropetrovskaya obl, Dnepr city, st. \
    Lazaryana,2, hostel 3, room 18.","dont_validate");
/* === */

	this.dataCollection = function (prev_tab) {
	this.hash_text = {"0" :[last_name,
													first_name,
													patronymic,
													day_birth,
													month_birth,
													year_birth,
													citizen,
													INN,
													sex,
													pasport_series,
													pasport_number,
													pasport_issued,
													date],
										"1" : [education,
													institution,
													diploma,
													year_ending,
													specialty,
													qvalify,
													educational_form],
										"2" : [institute_post,
													year_ending_post,
													acad_degree],
										"3" : [last_job,
													job_degree,
													leave,
													pension,
													family,
													family_degree,
													PIB,
													year_birthday],
										"4" : [partmen,
													pasport_partmen]};
	this.hash_checkbox =	{"0":[army],
											 "2":[aspirant,
														aduktur,
														doctor]};
		for (cont in this.hash_text) {

			for (i in this.hash_text[cont]) {
				if (prev_tab == cont) { 
				console.log(this.hash_text[cont][i].id,
										this.hash_text[cont][i].value);
				myCard.setValue(this.hash_text[cont][i].id,
												this.hash_text[cont][i].value);
				}	
			}
		}	
		for (cont in this.hash_checkbox) {

			for (i in this.hash_checkbox[cont]) {
				if (prev_tab == cont) { 
				console.log(this.hash_checkbox[cont][i].id,
										this.hash_checkbox[cont][i].checked);
  			myCard.setValue(this.hash_text[cont][i].id,
												this.hash_text[cont][i].checked);
				}	
			}
		}	
	}
  
  this.render_model = function(prev_tab) {
       
    var curr;
    var controller_education = new Controller_education(myCard.getValue("education_model"));
    var controller_family = new Controller_family(myCard.getValue("family_model"));
    controller_education.render_education();
    controller_education.render_post_education();
    controller_family.render_family();
    
	
		for (cont in this.hash_text) {

			for (i in this.hash_text[cont]) {
				if (prev_tab == cont) { 
				
					curr = myCard.getValue(this.hash_text[cont][i].id);
					if (curr !== undefined) {
						this.hash_text[cont][i].value = curr; 
						console.log(this.hash_text[cont][i].value);
					}
				
				}	
			}
		}

	
		for (cont in this.hash_checkbox) {

			for (i in this.hash_checkbox[cont]) {
				if (prev_tab == cont) { 
				
					curr = myCard.getValue(this.hash_text[cont][i].id);
					if (curr !== undefined) {
						this.hash_text[cont][i].checked = curr;  
					}
				
				}	
			}
		}	
	
	}

  

  
  this.render_preview = function() {
/*    var inputs_key = ["r_last_name","r_first_name","r_patronymic","day_birth","month_birth","r_year_birth",
    "citizen","r_pasport_series","r_pasport_number","pasport_issued","date",
  	"education","last_job","job_degree","leave","pension","family", "partmen","pasport_partmen"];
    var inputs_ch = [army,aspirant,aduktur,doctor];
    var inputs = [p_last_name, p_first_name, p_patronymic,p_day_birth,p_month_birth,p_year_birth,
      p_citizen,p_pasport_series,p_pasport_number,p_pasport_issued,p_date,
  		p_education,p_last_job,p_job_degree,p_leave,p_pension,p_family,
      p_partmen,p_pasport_partmen]; */
      
    var controller_education = new Controller_education(myCard.getValue("education_model"));
    var controller_family = new Controller_family(myCard.getValue("family_model"));
    controller_education.render_education();
    controller_education.render_post_education();
    controller_family.render_family();
    
/*    for (var i=0;i<inputs.length;i++) {
      curr = myCard.getValue(inputs_key[i]);
      if (curr !== undefined) {
        inputs[i].innerHTML = curr;  
      }
    }
    
    
    
    
    
    for (var i=0; i<inputs_ch.length; i++) {
  		curr = myCard.getValue(inputs_ch_key[i]);
			inputs_ch[i].checked = curr; 
		}
    
    */
  }

}